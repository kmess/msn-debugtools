/*
 * Simple program to convert an MSN "Context" field
 * to several display formats for reverse engineering.
 * 
 * Copyright (c) 2007 Diederik van der Boor <vdboor@codingdomain.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * Compile suggestion:
 * alias g++kde='g++ -L /opt/kde3/lib -I /opt/kde3/include/ -L /usr/lib/qt3/lib/ -I /usr/lib/qt3/include/ -l kdecore -l qt-mt'
 * g++kde -o contextdecoder contextdecoder.cpp
 *
 */


//#include <kdebug.h>

#include <kmdcodec.h>
#include <iostream>
using namespace std;


unsigned int extractBytes(const char *data, const int offset)
{
  // Convert the bytes from network order to a normal int.
  return (((unsigned char) data[offset + 0]      )
        | ((unsigned char) data[offset + 1] <<  8)
        | ((unsigned char) data[offset + 2] << 16)
        | ((unsigned char) data[offset + 3] << 24));
}


int main(int argc, char *argv[])
{
  if(argc != 2)
  {
    cerr << "Usage: " << argv[0] << " contextstring" << endl;
    return 1;
  }

  // Get context string
  QString context(argv[1]);

  // Remove base64 encoding
  QByteArray decodedContext;
  KCodecs::base64Decode(context.utf8(), decodedContext);

  // Extract data
  char *rawData = decodedContext.data();
  int   rawSize = decodedContext.size();

  // Interpret as Hex data
  const char hexMap[] = "0123456789abcdef";
  QString hex;
  for(int i = 0; i < rawSize; i++)
  {
    int upper = (rawData[i] & 0xf0) >> 4;
    int lower = (rawData[i] & 0x0f);
    hex += hexMap[upper];
    hex += hexMap[lower];
    if((i % 2) == 1) hex += " ";
  }

  // Interpret as UTF8
  QString ascii;
  for(int i = 0; i < rawSize; i++)
  {
    if(rawData[i] == 0)
    {
      ascii += ".";
    }
    else if(rawData[i] < 10)
    {
      ascii += " ";
    }
    else
    {
      const char oneChar = rawData[i];
      ascii += QString::fromUtf8(&oneChar, 1);
    }
  }

  // Interpret as UTF16
  QString utf16;
  if(context.contains("=="))
  {
    utf16 = QString::fromUcs2( reinterpret_cast<unsigned short*>(rawData) );
  }


  cout << "Result:" << endl;
  cout << "  Hex:   '" << hex << "'" << endl;
  cout << "  UTF8:  '" << ascii << "'" << endl;

  if(! utf16.isNull())
  {
    cout << "  UTF16: '" << utf16 << "'" << endl;
  }
  else
  {
    cout << "  UTF16: not available." << endl;
  }
}
