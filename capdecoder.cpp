/*
 * Simple program to convert an MSN "capabilities" field
 * to several display formats for reverse engineering.
 * 
 * Copyright (c) 2008 Diederik van der Boor <vdboor@codingdomain.com>
 *
 * This program is free software; you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation; either version 2 of the License, or
 * (at your option) any later version.
 *
 * This program is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with this program; if not, write to the Free Software
 * Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
 *
 *
 * Compile suggestion:
 * g++ capdecoder.cpp -o capdecoder -lQtCore -I /usr/lib/qt4/include/
 *
 */
#include <stdio.h>
#include <stdlib.h>
#include <QtCore/qstring.h>


int main( int argc, const char* argv[])
{
  if( argc != 2 )
  {
    fprintf( stderr, "usage: ./capdecoder 'context' %d\n" );
    return 1;
  }


  unsigned int caps = 0;
  QString capstr( argv[1] );
  if( capstr.startsWith("0x") )
  {
    fprintf(stderr, "sorry, hexadecimal notations are not supported yet.\n");
    return 1;
  }

  bool ok = false;
  caps = capstr.toUInt( &ok );
  if( ! ok )
  {
    fprintf(stderr, "error: capabilities string must be a number.\n");
    return 1;
  }

  unsigned int ver  = ( caps & 0xF0000000 );
  if( caps & 0x00000001 ) printf("0x00000001: is Windows mobile device\n");
  if( caps & 0x00000002 ) printf("0x00000002: ???\n");
  if( caps & 0x00000004 ) printf("0x00000004: supports INK in GIF format\n");
  if( caps & 0x00000008 ) printf("0x00000008: supports INK in ISF format\n");

  if( caps & 0x00000010 ) printf("0x00000010: has webcam connected\n");
  if( caps & 0x00000020 ) printf("0x00000020: supports multi-packet\n");
  if( caps & 0x00000040 ) printf("0x00000040: has mobile\n");
  if( caps & 0x00000080 ) printf("0x00000080: has direct mobile\n");

  if( caps & 0x00000100 ) printf("0x00000100: ???\n");
  if( caps & 0x00000200 ) printf("0x00000200: is web client\n");
  if( caps & 0x00000400 ) printf("0x00000400: ???\n");
  if( caps & 0x00000800 ) printf("0x00000800: is Offlice Live client\n");

  if( caps & 0x00001000 ) printf("0x00001000: has Windows Live Space\n");
  if( caps & 0x00002000 ) printf("0x00002000: is Media Center client\n");
  if( caps & 0x00004000 ) printf("0x00004000: supports direct-im\n");
  if( caps & 0x00008000 ) printf("0x00008000: supports winks\n");

  if( caps & 0x00010000 ) printf("0x00010000: supports msn-search\n");
  if( caps & 0x00020000 ) printf("0x00020000: is bot\n");
  if( caps & 0x00040000 ) printf("0x00040000: supports voice-clips\n");
  if( caps & 0x00080000 ) printf("0x00080000: supports secure channel\n");

  if( caps & 0x00100000 ) printf("0x00100000: supports sip invite\n");
  if( caps & 0x00200000 ) printf("0x00200000: ???\n");                    // since WLM 2009
  if( caps & 0x00400000 ) printf("0x00400000: supports folder sharing\n");
  if( caps & 0x00800000 ) printf("0x00800000: ???\n");

  if( caps & 0x01000000 ) printf("0x01000000: has onecare\n");
  if( caps & 0x02000000 ) printf("0x02000000: supports TURN as P2P transport layer\n");
  if( caps & 0x04000000 ) printf("0x04000000: supports bootstrapping P2P via UUN\n");
  if( caps & 0x08000000 ) printf("0x08000000: ???\n");

  switch( ver )
  {
    case 0: break;
    case 0x10000000: printf("0x10000000: supports MSNC1 / MSN 6.0.\n"); break;
    case 0x20000000: printf("0x20000000: supports MSNC2 / MSN 6.1.\n"); break;
    case 0x30000000: printf("0x30000000: supports MSNC3 / MSN 6.2.\n"); break;
    case 0x40000000: printf("0x40000000: supports MSNC4 / MSN 7.0.\n"); break;
    case 0x50000000: printf("0x50000000: supports MSNC5 / MSN 7.5.\n"); break;
    case 0x60000000: printf("0x60000000: supports MSNC6 / WLM 8.0.\n"); break;
    case 0x70000000: printf("0x70000000: supports MSNC7 / WLM 8.1.\n"); break;
    case 0x80000000: printf("0x80000000: supports MSNC8 / WLM 8.5.\n"); break;
    case 0x90000000: printf("0x90000000: supports MSNC9 / WLM 9.0 beta.\n"); break;
    case 0xa0000000: printf("0xa0000000: supports MSNC10 / WLM 2009.\n"); break;
    default:
    {
      printf("0x%x: unknown MSNC version\n", ver );
    }
  }

  return 0;
}
