#!/bin/sh
#
# Simple script to run KMess SVN and capture log data.
# 
# Copyright (c) 2007 Diederik van der Boor <vdboor@codingdomain.com>
#
# This program is free software; you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation; either version 2 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program; if not, write to the Free Software
# Foundation, Inc., 51 Franklin St, Fifth Floor, Boston, MA  02110-1301  USA
#
#
# The approach taken in this script allows faster output
# then KDevelop can handle, by outputting the logs to a file.
# The script is designed to be run from a kicker button,
# which the "Run in terminal" option selected.
# The opened Konsole automatically receives a nice title.
#
# After quiting KMess, a summary of all warnings
# will be given. 
#

# settings
#

LOG_FILE="$HOME/Desktop/KMess-SVN.log"
BIN_FILE="$HOME/development/kmess/build-debug/kmess/kmess"
AUTO_LOGIN="testaccount@yourdomain.org"



# Change console title, except in virtual consoles
case $TERM in
  xterm*|rxvt|Eterm|eterm|screen)
    echo -ne "\033]0;KMess SVN log\007"
    ;;
esac

#mv kmess.log kmess.log.1 -f
echo "=== Log started at `date` ===" > "$LOG_FILE"
tail -f "$LOG_FILE" &
$BIN_FILE --autologin "$AUTO_LOGIN" 2>&1 >> "$LOG_FILE"
kill %1 # kills tail -f

grep WARNING "$LOG_FILE" | grep -v "KLibrary: /usr/lib/libcrypto.so" > "$LOG_FILE.warnings"

if [ `filesize "$LOG_FILE.warnings"` = 0 ]
then
  # no errors
  (
    echo ""
    echo ""
    echo " ** completed execution, no errors found ** "
  ) >> "$LOG_FILE"

  rm "$LOG_FILE.warnings"
else
  # found errors
  (
    echo ""
    echo ""
    echo " ** completed execution, but errors found ** "
    cat "$LOG_FILE.warnings"
  ) >> "$LOG_FILE"

  # also display at the console
  echo ""
  echo ""
  echo " ** completed execution, but errors found ** "
  cat "$LOG_FILE.warnings"
  rm "$LOG_FILE.warnings"
  echo ""
  echo "press enter to quit"
  read dummy
  # read -n 1 dummy
fi


