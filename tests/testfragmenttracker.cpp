
#include "../../kmess/kmess/network/extra/p2pfragmenttracker.h"
#include <kdebug.h>
#include <iostream>

using namespace std;

int main()
{
  P2PFragmentTracker t;
  t.initialize( 23, 200 );

  cout << t.getDebugMap().utf8().data() << endl;
  t.registerFragment( 20, 40 );
  t.registerFragment( 0, 10 );
  t.registerFragment( 5, 15 );

  t.registerFragment( 90, 10 );
  t.registerFragment( 110, 10 );
  t.registerFragment( 80, 60 );

  t.registerFragment( 180, 20 );
  t.registerFragment( 170, 20 );
  //t.registerFragment( 0, 190 );

  cout << t.getDebugMap().utf8().data() << endl;
  return 0;
}

